use aws_sdk_dynamodb::{model::AttributeValue, Client, Error as DynamoError};

pub async fn increment_like(
    client: &Client,
    table: &str,
    channel_id: &str,
    timestamp: &str,
) -> Result<(), DynamoError> {
    let _response = client
        .update_item()
        .table_name(table)
        .key("channel_id", AttributeValue::S(channel_id.to_string()))
        .key(
            "timestamp_utc_iso8601",
            AttributeValue::S(timestamp.to_string()),
        )
        .update_expression("set likes = likes + :value")
        .expression_attribute_values(":value", AttributeValue::N("1".to_string()))
        .send()
        .await?;

    Ok(())
}
