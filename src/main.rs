pub mod tweet;
use lambda_http::{http::StatusCode, run, service_fn, Body, Error, Request, Response};
use tracing::info;

use aws_sdk_dynamodb::Client;

use tweet::increment_like;

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
struct RequestBody {
    timestamp: String,
}

/// This is the main body for the function.
/// Write your code inside it.
/// There are some code example in the following URLs:
/// - https://github.com/awslabs/aws-lambda-rust-runtime/tree/main/examples
async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let body = event.body();
    let s = std::str::from_utf8(&body).expect("invalid utf-8 sequence");
    //Log into Cloudwatch
    info!(payload = %s, "JSON Payload received");

    //Serialze JSON into struct.
    //If JSON is incorrect, send back 400 with error.
    let item = match serde_json::from_str::<RequestBody>(s) {
        Ok(item) => item,
        Err(err) => {
            let resp = Response::builder()
                .status(400)
                .header("content-type", "text/html")
                .body(err.to_string().into())
                .map_err(Box::new)?;
            return Ok(resp);
        }
    };
    let config = aws_config::load_from_env().await;

    let client = Client::new(&config);

    increment_like(&client, "dynamodb-all-messages", "rainbow", &item.timestamp).await?;

    let res = Response::builder()
        .status(StatusCode::NO_CONTENT)
        .body(Body::Empty)
        .map_err(Box::new)?;

    Ok(res)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
